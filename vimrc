set nocompatible
set encoding=utf-8

filetype plugin indent on
syntax enable

let mapleader=","
let maplocalleader=","


nmap <silent> <leader>ev :e  $MYVIMRC<cr>
nmap <silent> <leader>sv :so $MYVIMRC<cr>
au! BufWritePost $MYVIMRC source $MYVIMRC

" Manage plugins (use single quotes)
call plug#begin('~/.vim/plugged')

Plug 'Valloric/YouCompleteMe', { 'do': './install.sh --clang-completer' }
Plug 'scrooloose/syntastic', { 'for' : ['erlang', 'python'] }
Plug 'ntpeters/vim-better-whitespace'
Plug 'junegunn/vim-easy-align'
Plug 'Lokaltog/vim-easymotion'
Plug 'airblade/vim-gitgutter'
Plug 'majutsushi/tagbar'
Plug 'tpope/vim-fugitive'

Plug 'ten0s/syntaxerl', { 'for' : 'erlang' }
Plug 'jimenezrick/vimerl', { 'for' : 'erlang' }

Plug 'jmcantrell/vim-virtualenv', { 'for' : 'python' }

Plug 'Shougo/vimproc.vim', { 'do': 'make'}
Plug 'Shougo/unite.vim'
Plug 'rking/ag.vim'

Plug 'kien/ctrlp.vim'
Plug 'ctrlp-py-matcher'

Plug 'altercation/vim-colors-solarized'
Plug 'ciaranm/inkpot'
Plug 'sheerun/vim-wombat-scheme'
Plug 'bling/vim-airline'

call plug#end()


if has("gui_running")
    set background=dark
    colorscheme solarized

    set guifont=Powerline\ Consolas:h11,Inconsolata\ for\ Powerline:h11
    set lines=55 columns=100

    set guioptions+=a   " autoselect
    set guioptions+=c   " use console dialogs
endif


" junegunn/fzf
set rtp+=/usr/local/Cellar/fzf/0.9.2


""" Reading/Writing
set nobackup nowritebackup noswapfile
set noautowrite noautowriteall noautoread
set backspace=indent,eol,start
set hidden
set scrolloff=3
set showmatch matchtime=2
set autochdir


""" Searching
set ignorecase
set smartcase
set hlsearch
set incsearch
nmap <silent> <leader>/ :nohlsearch<cr>

set mouse=nicr

set number
set numberwidth=1

set shortmess+=a
set laststatus=2
set ruler
set showcmd
set wildmenu

" Whitespace
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab
set autoindent
set smarttab

set textwidth=78
set formatoptions=croqj

if (exists('+colorcolumn'))
    set colorcolumn=80
    highlight ColorColumn ctermbg=9
endif

"""" Command Line
set wildmenu                            " Autocomplete features in the status bar
set wildmode=list:longest
set wildignore+=*.o,*.obj,*.bak,*.exe,*.pyc,*.swp   " we don't want to edit these type of files



let g:ctrlp_match_func = { 'match': 'pymatcher#PyMatch' }


" fix logically wrapped lines
noremap j gj
noremap k gk


imap jj <esc>
nmap <leader>p :set invpaste paste?<cr>
nmap <leader>rr :1,$retab<rc>

map - :Explore<cr>
cmap w!! %!sudo tee > /dev/null %

" bind ctrl+space for omnicompletion
inoremap <Nul> <C-x><C-o>
imap <c-space> <c-x><c-o>
" allow arrow keys when code completion window is up
inoremap <Down> <C-R>=pumvisible() ? "\<lt>C-N>" : "\<lt>Down>"<CR>

" Easy align interactive
vnoremap <silent> <Enter> :EasyAlign<cr>

if &diff
    " easily handle diffing
    vnoremap < :diffget<CR>
    vnoremap > :diffput<CR>
else
    " visual shifting (builtin-repeat)
    vnoremap < <gv
    vnoremap > >gv
endif

" git bindings for Fugitive
nmap <leader>gs :Gstatus<cr>
nmap <leader>gc :Gcommit<cr>
nmap <leader>ga :Gcommit<cr>
nmap <leader>gl :Glog<cr>
nmap <leader>gd :Gdiff<cr>


" * to search the word in all files in dir
nmap * :Ag <c-r>=expand("<cword>")<cr><cr>
" seach in files
nnoremap <space>/ :Ag


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" tagbar
nmap <F4> :TagbarToggle<cr>


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" airline
let g:airline_theme='wombat'
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#branch#enabled = 1
let g:airline#extensions#syntastic#enabled = 1
let g:airline#extensions#tagbar#enabled = 1
let g:airline#extensions#hunk#enabled = 1
let g:airline#extensions#virtualenv#enabled = 1
let g:airline#extensions#whitespace#enabled = 1

if has("gui_running")
    let g:airline_powerline_fonts = 1
else
    let g:airline_powerline_fonts = 0
    let g:airline_symbols = {}
    let g:airline_symbols.linenr = '␊'
    let g:airline_symbols.linenr = '␤'
    let g:airline_symbols.linenr = '¶'
    let g:airline_symbols.branch = '⎇'
    let g:airline_symbols.paste = 'ρ'
    let g:airline_symbols.paste = 'Þ'
    let g:airline_symbols.paste = '∥'
    let g:airline_symbols.whitespace = 'Ξ'
endif


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" Fugitive
nnoremap <leader>ga :Git add %:p<cr><cr>
nnoremap <leader>gs :Gstatus<cr>
nnoremap <leader>gc :Gcommit -v -q<CR>
nnoremap <leader>gt :Gcommit -v -q %:p<CR>
nnoremap <leader>gd :Gdiff<CR>
nnoremap <leader>ge :Gedit<CR>
nnoremap <leader>gr :Gread<CR>
nnoremap <leader>gw :Gwrite<CR><CR>
nnoremap <leader>gl :silent! Glog<CR>:bot copen<CR>
nnoremap <leader>gp :Ggrep<Space>
nnoremap <leader>gm :Gmove<Space>
nnoremap <leader>gb :Git branch<Space>
nnoremap <leader>go :Git checkout<Space>
nnoremap <leader>gps :Dispatch! git push<CR>
nnoremap <leader>gpl :Dispatch! git pull<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" YCM
let g:ycm_autoclose_preview_window_after_insertion = 1


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" Unite
let g:unite_source_history_yank_enable = 1
try
    let g:unite_source_rec_async_comand='ag --nocolor --nogroup -g ""'
    call unite#filters#matcher_default#use(['matcher_fuzzy'])
catch
endtry
" search a file in the filetree
nnoremap <space><space> :split<cr> :<C-u>Unite -start-insert file_rec/async<cr>
" reset not it is <C-l> normally
:nnoremap <space>r <Plug>(unite_restart)


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" Erlang
let g:erlangCompleteFile = " ~/vim/plugged/vimerl/autoload/erlang_complete.erl"
let g:syntastic_erlang_checkers=['syntaxerl']



""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" Python
let g:syntastic_python_checkers=['prospector', 'pylint', 'pep8']


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
""" C/C++

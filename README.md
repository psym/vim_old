Installation:

    git clone into ~/.vim

Create symlinks:

    ln -s ~/.vim/vimrc ~/.vimrc

Install plugins:

    :PlugInstall

Extra packages required (maybe):
    python: prospector pep8 pyflakes pylint